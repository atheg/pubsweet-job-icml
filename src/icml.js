const tmp = require('tmp-promise')
const fs = require('fs')
const path = require('path')
const { execSync } = require('child_process')

const icmlHandler = async job => {
  // console.log('processing job', job.data.docx)
  // const buf = Buffer.from(job.data.docx.data, 'base64')
  const editoriaHtml = job.data.editoriaHtml.data

  const { path: tmpDir, cleanup } = await tmp.dir({
    prefix: '_conversion-',
    unsafeCleanup: true,
    dir: process.cwd(),
  })

  // console.log('Write the input to a temporary file')
  fs.writeFileSync(path.join(tmpDir, job.data.editoriaHtml.name), editoriaHtml)

  // console.log('Unzip that docx')
  // execSync(`unzip -o ${tmpDir}/${job.data.editoriaHtml.name} -d ${tmpDir}`)

  // console.log('Run XSL for HTML to ICML conversion')
  execSync(`bash ${path.resolve(__dirname, 'run_icml_conversion.sh')} ${tmpDir}`)

  // console.log('Return the HTML5 output')
  const icml = fs.readFileSync(
    path.join(tmpDir, 'outputs', 'editoria_book.icml'),
    'utf8',
  )

  await cleanup()

  return { icml }
}

const handleJobs = async () => {
  const {
    jobs: { connectToJobQueue },
  } = require('pubsweet-server')

  const jobQueue = await connectToJobQueue()

  // Subscribe to the job queue with an async handler
  await jobQueue.subscribe('xsweet-*', icmlHandler)
}

handleJobs()
