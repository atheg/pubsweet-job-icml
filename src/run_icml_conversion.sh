#!/bin/bash

# Directory of script
DIRECTORY=$(cd `dirname $0` && pwd)

# Modified from https://gitlab.coko.foundation/XSweet/XSweet_runner_scripts/raw/master/execute_chain.sh

# For producing HTML5 outputs via XSweet XSLT from sources extracted from .docx (Office Open XML)

# is any short identifier
TEMP=$1

ICMLDIR=$(find ${DIRECTORY}/.. -maxdepth 1  -name "editoria_html_to_icml*" -print -quit)

SAXONDIR=$(find ${DIRECTORY}/.. -maxdepth 1  -name 'saxon' -print -quit)

saxonHE="java -jar ${SAXONDIR}/saxon9he.jar"  # SaxonHE (XSLT 3.0 processor)

ICML="${ICMLDIR}/editoria_html_to_icml.xml"

$saxonHE -xsl:$ICML -s:$TEMP/editoriaHtml.html -o:$TEMP/outputs/editoria_book.icml
