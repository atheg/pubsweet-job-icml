FROM node:11

WORKDIR /home/node

RUN apt-get update \
    && apt-get install -y software-properties-common unzip

RUN add-apt-repository 'deb http://http.debian.net/debian stretch-backports main' \
    && apt-get update \
    && apt-get install -y openjdk-11-jre-headless

# Install JS dependencies

COPY package.json .
RUN yarn

# Download ICML XSL
RUN wget https://gitlab.coko.foundation/atheg/editoria-html-to-icml/repository/archive.zip?ref=master -O xsweet.zip; unzip xsweet.zip; rm xsweet.zip

# Download Saxon
RUN wget "https://downloads.sourceforge.net/project/saxon/Saxon-HE/9.8/SaxonHE9-8-0-1J.zip" -O saxon.zip; unzip saxon.zip -d saxon; rm saxon.zip

COPY . .

# Test conversion
RUN mkdir _test
RUN cp test.html _test/test.html
RUN ./src/run_icml_conversion.sh _test
RUN grep -q 'Testing conversion 123' _test/outputs/editoria_book.icml
RUN rm -rf _test

# Start job processing
CMD node src/icml.js
